/*******************************************************************************
 * Copyright (c) 2017, Kyle Robbertze <krobbertze@gmail.com>
 *
 * This project is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Represents a single organism in an evolutionary algorithm
 * 
 *        Created:  09/02/2017 10:07:25
 *       Compiler:  g++
 * 
 *         Author:  Kyle Robbertze (kr), krobbertze@gmail.com
 *******************************************************************************/
#include <iostream>

#include <stdlib.h>
#include <cmath>

#include "organism.h"

using namespace gat;

Organism::Organism() :
    m_dna(nullptr),
    m_fitness(0),
    m_mated(false)
{ }

Organism::Organism(const binaryData_t& dna) :
    Organism()
{
    m_dna = new binaryData_t(dna);
}

Organism::~Organism() {
    if (m_dna)
        delete m_dna;
    m_dna = nullptr;
}

Organism::Organism(const Organism& other) :
    Organism(*other.m_dna)
{
    m_fitness = other.m_fitness;
    m_mated = other.m_mated;
}

binaryData_t* Organism::dna() {
    return m_dna;
}

void Organism::setDna(binaryData_t* dna) {
    if (m_dna)
        delete m_dna;
    m_dna = dna;
}

int Organism::fitness() const {
    return m_fitness;
}

void Organism::setFitness(const int fitness) {
    m_fitness = fitness;
}

bool Organism::hasMated() const {
    return m_mated;
}

Organism Organism::mate(Organism& mate, double mutationConstant) {
    binaryData_t dna = binaryData_t(*m_dna);
    uint_t crossingIndex = std::rand() % (dna.size());
    uint_t halfLength = (uint_t)std::round(dna.size() / 2.0);
    for (uint_t i = 0; i < halfLength; i++) {
        uint_t index = crossingIndex + i < dna.size() && crossingIndex + i < mate.dna()->size() ?
            crossingIndex + i : i;
        dna.at(index) = mate.dna()->at(index);
    }

    int mutationCount = std::rand() %
        (2 * (int)std::round(mutationConstant + dna.size()));
    for (int i = 0; i < mutationCount; i++) {
        int mutatedBasePair = std::rand() % (dna.size() * 8);
        unsigned char basePairBuffer = dna.at(mutatedBasePair / 8);
        if (basePairBuffer& (unsigned char)std::pow(2.0, mutatedBasePair % 8))
            dna.at(mutatedBasePair / 8) = basePairBuffer ^
                (unsigned char)std::pow(2.0, mutatedBasePair % 8);
        else
            dna.at((int)(mutatedBasePair / 8)) = basePairBuffer |
                (unsigned char)std::pow(2.0, mutatedBasePair % 8);
    }
    Organism child = Organism(dna);
    return child;
}

bool Organism::operator<(const Organism& org) const {
    return m_fitness < org.m_fitness;
}

bool Organism::operator>(const Organism& org) const {
    return m_fitness > org.m_fitness;
}

Organism& Organism::operator=(const Organism& org) {
    if (m_dna)
        delete m_dna;
    m_dna = new binaryData_t(*org.m_dna);
    m_fitness = org.m_fitness;
    m_mated = org.m_mated;
    return *this;
}
