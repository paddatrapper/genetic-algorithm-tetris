/*******************************************************************************
 * Copyright (c) 2017, Kyle Robbertze <krobbertze@gmail.com>
 *
 * This project is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Performs actions on a Tetris board
 * 
 *        Created:  09/02/2017 10:07:25
 *       Compiler:  g++
 * 
 *         Author:  Kyle Robbertze (kr), krobbertze@gmail.com
 *******************************************************************************/
#include <iostream>

#include <climits>
#include <cmath>
#include <cstring>

#include "gat_agent.h"

using namespace gat;

GATAgent::GATAgent(int* actionList, unsigned int size) :
    m_actionList(actionList),
    m_size(size),
    m_move(0)
{ }

GATAgent::~GATAgent() { }

Action GATAgent::getAction(Tetris* board) {
    if (m_move < m_size) {
        if (std::abs(m_actionList[m_move]) >= Rotation::NUM_ROTATIONS ||
                m_actionList[m_move] == INT_MIN)
            m_actionList[m_move] = std::rand() % Rotation::NUM_ROTATIONS;
        Rotation r = static_cast<Rotation>(m_actionList[m_move]);
        if (std::abs(m_actionList[m_move + 1]) > board->highestValidColWithRot(r) ||
                m_actionList[m_move + 1] == INT_MIN)
            while (std::abs(m_actionList[m_move + 1]) > board->highestValidColWithRot(r) ||
                    m_actionList[m_move + 1] == INT_MIN)
                m_actionList[m_move + 1] = std::rand() % TETRIS_COLS;
        int column = m_actionList[m_move + 1];
        m_move += 2;
        Action a = Action(r, column);
        return a;
    }
    int* newList = new int[m_size + 2];
    std::memcpy(newList, m_actionList, m_size);
    delete m_actionList;
    m_actionList = newList;
    m_size += 2;
    m_actionList[m_move] = 0;
    m_actionList[m_move + 1] = 0;
    Action a = Action(Rotation::NONE, 0);
    m_move += 2;
    return a;
}

int* GATAgent::actionList() {
    return m_actionList;
}

int GATAgent::actionListSize() {
    return m_size;
}
