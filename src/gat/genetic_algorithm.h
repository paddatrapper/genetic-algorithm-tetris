/*******************************************************************************
 * Copyright (c) 2017, Kyle Robbertze <krobbertze@gmail.com>
 *
 * This project is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Represents a single organism in an evolutionary algorithm
 * 
 *        Created:  09/02/2017 10:07:25
 *       Compiler:  g++
 * 
 *         Author:  Kyle Robbertze (kr), krobbertze@gmail.com
 *******************************************************************************/

#ifndef GENETIC_ALGORITHM_H
#define GENETIC_ALGORITHM_H

#define MATE_VARIANCE_CONSTANT 0.3
#define MUTATION_PER_BASE_PAIR_CONSTANT 0.1
#define MAXIMUM_POPULATION 10

#include <vector>

#include "organism.h"

namespace gat {
    using generation_t = std::vector<Organism>;
    using uint_t = unsigned int;

    class GeneticAlgorithm {
        public:
            GeneticAlgorithm();
            GeneticAlgorithm(generation_t organisms, int desiredFitness,
                    int (*testFitness)(Organism&), void (*showProgress)(int));
            Organism evolve();
            GeneticAlgorithm& operator=(const GeneticAlgorithm& other);
        private:
            void reproduce(int totalFitness);
            void showProgress(int fitness);
            int testOrganisms();
            generation_t m_organisms;
            int m_desiredFitness;
            int (*m_testFitness)(Organism&);
            void (*m_showProgress)(int);
    };
}
#endif // GENETIC_ALGORTIHM_H
