/*******************************************************************************
 * Copyright (c) 2017, Kyle Robbertze <krobbertze@gmail.com>
 *
 * This project is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Represents a single organism in an evolutionary algorithm
 * 
 *        Created:  09/02/2017 10:07:25
 *       Compiler:  g++
 * 
 *         Author:  Kyle Robbertze (kr), krobbertze@gmail.com
 *******************************************************************************/

#ifndef ORGANISM_H
#define ORGANISM_H

#include <vector>

namespace gat {
    using binaryData_t = std::vector<unsigned char>;
    using uint_t = unsigned int;

    class Organism {
        public:
            Organism();
            Organism(const binaryData_t& dna);
            Organism(const Organism& other);
            ~Organism();
            binaryData_t* dna();
            void setDna(binaryData_t* dna);
            int fitness() const;
            void setFitness(const int fitness);
            bool hasMated() const;
            Organism mate(Organism& mate, double mutationConstant);

            bool operator<(const Organism& org) const;
            bool operator>(const Organism& org) const;
            Organism& operator=(const Organism& org);
        private:
            binaryData_t* m_dna;
            uint_t m_fitness;
            bool m_mated;

    };
}

#endif // ORGANISM_H
