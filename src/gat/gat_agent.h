/*******************************************************************************
 * Copyright (c) 2017, Kyle Robbertze <krobbertze@gmail.com>
 *
 * This project is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Performs actions on a Tetris board
 * 
 *        Created:  09/02/2017 10:07:25
 *       Compiler:  g++
 * 
 *         Author:  Kyle Robbertze (kr), krobbertze@gmail.com
 *******************************************************************************/

#ifndef GAT_AGENT_H
#define GAT_AGENT_H

#include <vector>

#include "tetris/Agent.h"
#include "tetris/Action.h"

namespace gat {
    class GATAgent : public Agent {
        public:
            GATAgent(int* actionList, unsigned int size);
            ~GATAgent() override;
            Action getAction(Tetris* board) override; 
            int* actionList();
            int actionListSize();
        private:
            int* m_actionList;
            unsigned int m_size;
            unsigned int m_move;
    };
}
#endif // GAT_AGENT_H
