/*******************************************************************************
 * Copyright (c) 2017, Kyle Robbertze <krobbertze@gmail.com>
 *
 * This project is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Represents a single organism in an evolutionary algorithm
 * 
 *        Created:  09/02/2017 10:07:25
 *       Compiler:  g++
 * 
 *         Author:  Kyle Robbertze (kr), krobbertze@gmail.com
 *******************************************************************************/
#include <iostream>

#include <algorithm>
#include <stdlib.h>
#include <time.h>

#include "genetic_algorithm.h"

using namespace gat;

GeneticAlgorithm::GeneticAlgorithm() :
    m_organisms(generation_t()),
    m_desiredFitness(0),
    m_testFitness(nullptr),
    m_showProgress(nullptr)
{ }
    
GeneticAlgorithm::GeneticAlgorithm(generation_t organisms, int desiredFitness,
        int (*testFitness)(Organism&), void (*showProgress)(int)) :
    m_organisms(organisms),
    m_desiredFitness(desiredFitness),
    m_testFitness(testFitness),
    m_showProgress(showProgress)
{ }

Organism GeneticAlgorithm::evolve() {
    if (m_organisms.size() % 2 || MAXIMUM_POPULATION % 2)
        throw "Uneven number of organisms or maximum population. Aborting!";
    int totalFitness {testOrganisms()};
    while (m_organisms.at(0).fitness() < m_desiredFitness) {
        reproduce(totalFitness);
        totalFitness = testOrganisms();
        m_showProgress(m_organisms.at(0).fitness());
    }
    return m_organisms.at(0);
}

int GeneticAlgorithm::testOrganisms() {
    uint_t totalFitness = 0;
    for (auto& organism : m_organisms) {
        int fitness {m_testFitness(organism)};
        fitness = fitness < 0 ? 0 : fitness;
        organism.setFitness(fitness);
        totalFitness += fitness;
    }
    std::sort(m_organisms.begin(), m_organisms.end(), std::greater<Organism>()); // TODO: Invalid pointer access. Access memory at/after end of m_organisms
    return totalFitness;
}

void GeneticAlgorithm::reproduce(int totalFitness) {
    generation_t nextGeneration;
    for (uint_t i = 0; i < m_organisms.size() &&
            nextGeneration.size() < MAXIMUM_POPULATION; i++) {
        Organism& organism {m_organisms.at(i)};
        if (organism.hasMated())
            continue;
        int randomNumber {std::rand() %
            (int)(m_organisms.size() * MATE_VARIANCE_CONSTANT)};
        randomNumber = (i + randomNumber >= m_organisms.size()) ?
            m_organisms.size() - i - 1 : randomNumber;
        Organism& mate {m_organisms.at(i + randomNumber)};
        uint_t index = i;
        while (mate.hasMated()) {
            mate = m_organisms.at(index++);
        }
        uint_t children = (uint_t)(m_organisms.size() *
                (double)(organism.fitness() + mate.fitness()) / totalFitness);
        for (uint_t j = 0; j < children && nextGeneration.size() < MAXIMUM_POPULATION; j++) {
            Organism child =
                organism.mate(mate, MUTATION_PER_BASE_PAIR_CONSTANT);
            nextGeneration.push_back(child);
        }
    }
    if (nextGeneration.size() % 2) {
        binaryData_t dna = binaryData_t(nextGeneration.at(0).dna()->size());
        for (auto& byte : dna)
            byte = 0;
        Organism dummyOrg = Organism(dna);
        nextGeneration.push_back(dummyOrg);
    }
    m_organisms = nextGeneration;
}

GeneticAlgorithm& GeneticAlgorithm::operator=(const GeneticAlgorithm& other) {
    m_organisms = other.m_organisms;
    m_desiredFitness = other.m_desiredFitness;
    m_testFitness = other.m_testFitness;
    m_showProgress = other.m_showProgress;
    return *this;
}
