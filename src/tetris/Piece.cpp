/*******************************************************************************
 * Copyright (c) 2015 Matt Brenman
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/

#include "Piece.h"
#include "assert.h"
#include <iostream>
#include <ncurses.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

Piece::Piece()
{
	pieceID = (rand() % NUMPIECES) + 1;
	setPiece(pieceID);
}

Piece::Piece(int id)
{
	pieceID = id;
	setPiece(id);
}

void Piece::setPiece(int id)
{
	//Reserved id for empty spaces
	assert(id != 0);

	switch(pieceID){
		case 1:
			// . . . .
			// . . . .
			// X X . .
			// X X . .
				piece[0][0] = 0;
				piece[1][0] = 0;
				piece[2][0] = 0;
				piece[3][0] = 0;
				piece[0][1] = 0;
				piece[1][1] = 0;
				piece[2][1] = 0;
				piece[3][1] = 0;
				piece[0][2] = 1;
				piece[1][2] = 1;
				piece[2][2] = 0;
				piece[3][2] = 0;
				piece[0][3] = 1;
				piece[1][3] = 1;
				piece[2][3] = 0;
				piece[3][3] = 0;
			break;
		case 2:
			// . . . .
			// . . . .
			// X . . .
			// X X X .
				piece[0][0] = 0;
				piece[1][0] = 0;
				piece[2][0] = 0;
				piece[3][0] = 0;
				piece[0][1] = 0;
				piece[1][1] = 0;
				piece[2][1] = 0;
				piece[3][1] = 0;
				piece[0][2] = 1;
				piece[1][2] = 0;
				piece[2][2] = 0;
				piece[3][2] = 0;
				piece[0][3] = 1;
				piece[1][3] = 1;
				piece[2][3] = 1;
				piece[3][3] = 0;
			break;
		case 3:
			// . . . .
			// . . . .
			// . . X .
			// X X X .
				piece[0][0] = 0;
				piece[1][0] = 0;
				piece[2][0] = 0;
				piece[3][0] = 0;
				piece[0][1] = 0;
				piece[1][1] = 0;
				piece[2][1] = 0;
				piece[3][1] = 0;
				piece[0][2] = 0;
				piece[1][2] = 0;
				piece[2][2] = 1;
				piece[3][2] = 0;
				piece[0][3] = 1;
				piece[1][3] = 1;
				piece[2][3] = 1;
				piece[3][3] = 0;
			break;
		case 4:
			// . . . .
			// . . . .
			// . . . .
			// X X X X
				piece[0][0] = 0;
				piece[1][0] = 0;
				piece[2][0] = 0;
				piece[3][0] = 0;
				piece[0][1] = 0;
				piece[1][1] = 0;
				piece[2][1] = 0;
				piece[3][1] = 0;
				piece[0][2] = 0;
				piece[1][2] = 0;
				piece[2][2] = 0;
				piece[3][2] = 0;
				piece[0][3] = 1;
				piece[1][3] = 1;
				piece[2][3] = 1;
				piece[3][3] = 1;
			break;
		case 5:
			// . . . .
			// . . . .
			// . X X .
			// X X . .
				piece[0][0] = 0;
				piece[1][0] = 0;
				piece[2][0] = 0;
				piece[3][0] = 0;
				piece[0][1] = 0;
				piece[1][1] = 0;
				piece[2][1] = 0;
				piece[3][1] = 0;
				piece[0][2] = 0;
				piece[1][2] = 1;
				piece[2][2] = 1;
				piece[3][2] = 0;
				piece[0][3] = 1;
				piece[1][3] = 1;
				piece[2][3] = 0;
				piece[3][3] = 0;
			break;
		case 6:
			// . . . .
			// . . . .
			// X X . .
			// . X X .
				piece[0][0] = 0;
				piece[1][0] = 0;
				piece[2][0] = 0;
				piece[3][0] = 0;
				piece[0][1] = 0;
				piece[1][1] = 0;
				piece[2][1] = 0;
				piece[3][1] = 0;
				piece[0][2] = 1;
				piece[1][2] = 1;
				piece[2][2] = 0;
				piece[3][2] = 0;
				piece[0][3] = 0;
				piece[1][3] = 1;
				piece[2][3] = 1;
				piece[3][3] = 0;
			break;
		case 7:
			// . . . .
			// . . . .
			// X X . .
			// . X X .
				piece[0][0] = 0;
				piece[1][0] = 0;
				piece[2][0] = 0;
				piece[3][0] = 0;
				piece[0][1] = 0;
				piece[1][1] = 0;
				piece[2][1] = 0;
				piece[3][1] = 0;
				piece[0][2] = 0;
				piece[1][2] = 1;
				piece[2][2] = 0;
				piece[3][2] = 0;
				piece[0][3] = 1;
				piece[1][3] = 1;
				piece[2][3] = 1;
				piece[3][3] = 0;
			break;
		default:
			cerr << "BAD PIECE ID " << pieceID;
			exit(1);
	}
}

int Piece::getPieceID()
{
	return pieceID;
}

void Piece::rotate(Rotation rot)
{
	bool buf[PIECESIZE][PIECESIZE];
	copyPiece(buf);

	switch(rot){
		case NONE:
			// cout << "About to rotate NONE " << endl;
			break;
		case CLOCKWISE:
			// cout << "About to rotate CLOCKWISE " << endl;
			for (int x = 0; x < PIECESIZE; x++) {
				for (int y = 0; y < PIECESIZE; y++) {
					buf[PIECESIZE - y - 1][x] = piece[x][y];
				}
			}
			break;
		case COUNTER_CLOCKWISE:
			// cout << "About to rotate COUNTER_CLOCKWISE " << endl;
			for (int x = 0; x < PIECESIZE; x++) {
				for (int y = 0; y < PIECESIZE; y++) {
					buf[y][PIECESIZE - x - 1] = piece[x][y];
				}
			}
			break;
		case FLIP:
			// cout << "About to rotate FLIP " << endl;
			for (int x = 0; x < PIECESIZE; x++) {
				for (int y = 0; y < PIECESIZE; y++) {
					buf[PIECESIZE - x - 1][PIECESIZE - y - 1] = piece[x][y];
				}
			}
			break;
		default:
            std::cerr << "error: invalid rotation " << static_cast<int>(rot) << "\n";
			throw 1;
	}

	replacePiece(buf);
	pullLeft();
	pullDown();
}

void Piece::replacePiece(bool buf[PIECESIZE][PIECESIZE])
{

	for (int x = 0; x < PIECESIZE; x++) {
		for (int y = 0; y < PIECESIZE; y++) {
			piece[x][y] = buf[x][y];
			assert(piece[x][y] == buf[x][y]);
		}
	}
}

void Piece::pullLeft()
{
	while (emptyLeft()){
		for (int y = 0; y < PIECESIZE; y++) {
			for (int x = 0; x < PIECESIZE - 1; x++) {
				piece[x][y] = piece[x + 1][y];
			}
			piece[PIECESIZE - 1][y] = 0;
		}
	}
}

bool Piece::emptyLeft()
{
	for (int y = 0; y < PIECESIZE; y++) {
		if (piece[0][y]){
			return false;
		}
	}
	return true;
}

void Piece::pullDown()
{
	while (emptyBottom()){
		for (int x = 0; x < PIECESIZE; x++) {
			for (int y = PIECESIZE - 1; y > 0; y--) {		
				piece[x][y] = piece[x][y - 1];
			}
			piece[x][0] = 0;
		}
	}
}

bool Piece::emptyBottom()
{
	for (int x = 0; x < PIECESIZE; x++) {
		if (piece[x][PIECESIZE - 1]){
			return false;
		}
	}
	return true;
}

void Piece::copyPiece(bool buf[PIECESIZE][PIECESIZE])
{
	for (int x = 0; x < PIECESIZE; x++) {
		for (int y = 0; y < PIECESIZE; y++) {
			buf[x][y] = piece[x][y];
		}
	}
}

int Piece::pieceWidth()
{
	int width = 0;
	for (int x = 0; x < PIECESIZE; x++) {
		for (int y = 0; y < PIECESIZE; y++) {
			if (piece[x][y]) {
				width = x;
			}
		}
	}
	return width;
}

void Piece::printPiece(int start_y, int start_x)
{
	for (int y = 0; y < PIECESIZE; y++) {
		for (int x = 0; x < PIECESIZE; x++) {
			if (piece[x][y]) {
                attron(COLOR_PAIR(pieceID));
				mvprintw(start_y + y, start_x + x, "  ");
                attroff(COLOR_PAIR(pieceID));
			} else {
				mvprintw(start_y + y, start_x + x, ". ");
			}
		}
	}
}
