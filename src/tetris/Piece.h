/*******************************************************************************
 * Copyright (c) 2015 Matt Brenman
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/

#ifndef __Piece__
#define __Piece__

#include "Constants.h"

class Piece {
	public:
		Piece();
		Piece(int id);
		void copyPiece(bool buf[PIECESIZE][PIECESIZE]);
		void printPiece(int y, int x);
		int pieceWidth();
		int getPieceID();
		void rotate(Rotation rot);
	private:
		bool piece[PIECESIZE][PIECESIZE];
		int pieceID;
		void setPiece(int id);
		void pullLeft();
		void pullDown();
		void replacePiece(bool buf[PIECESIZE][PIECESIZE]);
		bool emptyLeft();
		bool emptyBottom();
};

#endif
