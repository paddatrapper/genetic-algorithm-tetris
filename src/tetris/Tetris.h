/*******************************************************************************
 * Copyright (c) 2015 Matt Brenman
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/

#ifndef __Tetris__
#define __Tetris__

#include "Constants.h"
#include "Piece.h"
#include "Action.h"
#include <assert.h>
#include <unistd.h>

const int EMPTY_SPACE =  0;
const int RESERVED    = -1;

const int ANIM_DELAY = 0;

class Tetris {
	public:
	    Tetris(bool official);
	  	Tetris(const int board[TETRIS_COLS][TETRIS_ROWS], int pieceID, int nextPieceID, int cleared);
	    void printBoard(bool showData = true);
	    bool isLost();
	    Piece* currentPiece();
	    void setPiece(int id);
	    void playAction(Action a, bool animated, bool showData = true);
	    int highestValidCol();
	    int highestValidColWithRot(Rotation rot);
	    Tetris* gameCopy();
	    int getLinesCleared();
	    int maxBoardHeight();
	    void copyBoard(int dest[TETRIS_COLS][TETRIS_ROWS]);
	    ~Tetris();
	private:
		int board[TETRIS_COLS][TETRIS_ROWS];
		int linesCleared;
		bool gameover;
		const bool officialGame;
		Piece* curPiece;
		Piece* nextPiece;
		void clearBoard();
		bool collision(int dropCol, int dropRow);
		void placePiece(int dropCol, int dropRow, int inBoard[TETRIS_COLS][TETRIS_ROWS], bool activeBoard);
		void dropInColumn(int col, bool animated);
		void clearLines();
		bool lineIsFull(int y);
		void clearLine(int startCol);
		void printSpecial(int inBoard[TETRIS_COLS][TETRIS_ROWS]);
		void printBoardWithDroppingPiece(int col, int dropRow);
		void printData();
};


#endif
