/*******************************************************************************
 * Copyright (c) 2017, Kyle Robbertze <krobbertze@gmail.com>
 *
 * This project is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Represents a single organism in an evolutionary algorithm
 * 
 *        Created:  09/02/2017 10:07:25
 *       Compiler:  g++
 * 
 *         Author:  Kyle Robbertze (kr), krobbertze@gmail.com
 *******************************************************************************/
#ifndef USE_NCURSES
#define USE_NCURSES 0
#endif
#define MAX_MOVES 50

#include <climits>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ncurses.h>
#include <stdlib.h>
#include <sstream>
#include <string>

#include "gat/gat_agent.h"
#include "gat/genetic_algorithm.h"
#include "gat/organism.h"

#include "tetris/Agent.h"
#include "tetris/Tetris.h"


void playGame(Tetris* board, Agent* player, bool animated) {
    int i = 0;
    while (!board->isLost() && i++ < MAX_MOVES) {
        Action a = player->getAction(board);
        board->playAction(a, animated, false);
    }
}

void showNcursesProgress(int fitness) {
    char msg[18]; // strlen("Fitness: ") + 7;
    sprintf(msg, "Fitness: %7d", fitness);

    mvprintw(TETRIS_ROWS + 1, 0, "%s", msg);
}

void showConsoleProgress(int fitness) {
    std::cout << "Fitness: " << fitness << "\n";
}

int testFitness(gat::Organism& organism) {
    srand(1);
    Tetris* board = new Tetris(true);
    Agent* player = new gat::GATAgent((int*)organism.dna()->data(), organism.dna()->size() / sizeof(int));
    playGame(board, player, USE_NCURSES);
    int fitness = board->getLinesCleared() * 100 + 1;
    gat::GATAgent* gat = (gat::GATAgent*) player;
    int* data = gat->actionList();
    gat::binaryData_t* dna = new gat::binaryData_t(gat->actionListSize() * sizeof(int));
    std::memcpy(dna->data(), data, dna->size());
    organism.setDna(dna);
    delete board;
    delete player;
    return fitness;
}

int main() {
    srand(1);
    gat::generation_t organisms = gat::generation_t {4};
    for (auto& organism : organisms) {
        int data[] = { 0, 0 };
        gat::binaryData_t* dna = new gat::binaryData_t(2 * sizeof(int));
        std::memcpy(dna->data(), data, dna->size());
        organism.setDna(dna);
    }
    int desiredFitness = 10000000;
    gat::GeneticAlgorithm ga;
    if (USE_NCURSES) {
        initscr();
        start_color();
        curs_set(0);
        for (int i = 1; i < 8; i++)
            init_pair(i, COLOR_BLACK, i);
        ga = gat::GeneticAlgorithm(organisms, desiredFitness, testFitness, showNcursesProgress);
    } else
        ga = gat::GeneticAlgorithm(organisms, desiredFitness, testFitness, showConsoleProgress);

    gat::Organism result = ga.evolve();
    if (USE_NCURSES) {
        getch();
        endwin();
    }
    return EXIT_SUCCESS;
}
